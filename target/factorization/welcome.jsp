<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Factorization</title>
    <meta name="description" content="Factorization">
    <meta name="keywords" content="factorization">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>">
</head>
<body>
<div class="container-fluid">
<a href="factorize" title="Разложение на множители">Перейти на страничку для разложения чисел на простые множители</a>
</div>
</body>
</html>
