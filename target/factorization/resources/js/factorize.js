$(document).on('click', '.dev_add_button', function () {
    var m = $(".dev_input_block_item_template").clone();
    $(m).removeClass('dev_input_block_item_template');
    $('.dev_input_block').append($(m).show());
});

$(document).on('click', '.dev_delete_button', function () {
    $(this).closest('.dev_input_block_item').remove();
});

$(document).on('click', '.dev_factorization', function () {
    //разложить:)
    factorizate();
});

function factorizate() {
    if ($('.dev_factorization').hasClass('loading')) {
        return;
    }
    $('.dev_factorization').addClass('loading');
    //var responseList = [];
    $(".dev_input_block .dev_input_number").each(function (i, o) {
        var success = true;
        if (typeof($(o).val()) == 'undefined' || $(o).val() == "" || $(o).val() == 0) {
            success = false;
        }
        if (success) {
            var responseItem = {};
            responseItem['value'] = Math.abs($(o).val() * 1);
            var dataStr = JSON.stringify(responseItem);
            $.ajax({
                url: "factorize",
                data: dataStr,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                success: function (answer) {
                    if (answer) {
                        if (answer.status === 'SUCCESS') {
                            var string = '';
                            for (var i = 0; i < answer.data.length; i++) {
                                string = string + ' ' + answer.data[i].andIncrement;
                            }
                            $(o).closest('.dev_input_block_item').find('.dev_result_label').show();
                            $(o).closest('.dev_input_block_item').find('.dev_result').text(string);
                        } else {
                            $(o).closest('.dev_input_block_item').find('.dev_result_label').show();
                            $(o).closest('.dev_input_block_item').find('.dev_result').text('непредвиденная ошибка');
                        }
                    }

                }
            });
        } else {
            $(o).closest('.dev_input_block_item').find('.dev_result_label').show();
            $(o).closest('.dev_input_block_item').find('.dev_result').text('проверьте корректность введенного значения');
        }
        $('.dev_factorization').removeClass('loading');

    });


}

