package test;

import main.java.service.FactorizeService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Olesya on 12.05.2017.
 */
@Controller
public class FactorizeTest {

    private FactorizeService factorizeService;

    @Autowired
    public void setFactorizeService(FactorizeService factorizeService) {
        this.factorizeService = factorizeService;
    }

    private List<Integer> list(int... factors) {
        List<Integer> listOfFactors = new ArrayList<Integer>();
        for (int i : factors) {
            listOfFactors.add(i);
        }
        return listOfFactors;
    }

    @Test
    public void testOne() {
        assertEquals(list(), factorizeService.doFactorizate(BigInteger.ONE));
    }

    @Test
    public void testTwo() {
        assertEquals(list(2), factorizeService.doFactorizate(BigInteger.valueOf(2)));
    }

    @Test
    public void testThree() {
        assertEquals(list(3), factorizeService.doFactorizate(BigInteger.valueOf(3)));
    }

    @Test
    public void testFour() {
        assertEquals(list(2, 2), factorizeService.doFactorizate(BigInteger.valueOf(4)));
    }

    @Test
    public void testSeventyTwo() {
        assertEquals(list(2, 2, 2, 3, 3), factorizeService.doFactorizate(BigInteger.valueOf(72)));
    }
}


