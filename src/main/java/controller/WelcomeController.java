package main.java.controller;



import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Olesya on 15.05.2017.
 */
@Controller
@RequestMapping(value = "/welcome")
public class WelcomeController {
    private static final String JSP_NAME = "welcome";

    @RequestMapping(method = RequestMethod.GET)
    public String showIndex(Model model) {
        return JSP_NAME;
    }


}
