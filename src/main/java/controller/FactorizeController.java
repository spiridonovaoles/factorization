package main.java.controller;


import main.java.beans.AtomicBigInteger;
import main.java.beans.ResponseData;
import main.java.beans.data.Answer;
import main.java.beans.data.Status;
import main.java.service.FactorizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Olesya on 06.05.2017.
 */
@Controller
@RequestMapping(value = "/factorize")
public class FactorizeController {
    private static final String JSP_NAME = "factorize";

    private FactorizeService factorizeService;

    @Autowired
    public void setFactorizeService(FactorizeService factorizeService) {
        this.factorizeService = factorizeService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public String showIndex(Model model) {
        return JSP_NAME;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Answer factorizate(Model model, @RequestBody(required = false) ResponseData data) {
        CopyOnWriteArrayList<AtomicBigInteger> primeFactors = factorizeService.doFactorizate(data.getValue());
        return new Answer(Status.SUCCESS, "Успешно", primeFactors);
    }
}
