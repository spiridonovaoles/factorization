package main.java.beans;

import java.math.BigInteger;

/**
 * Created by Olesya on 07.05.2017.
 */
public class ResponseData {

    private BigInteger value;

    public BigInteger getValue() {
        return value;
    }

    public void setValue(BigInteger value) {
        this.value = value;
    }
}
