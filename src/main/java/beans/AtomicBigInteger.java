package main.java.beans;

import java.math.BigInteger;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Olesya on 14.05.2017.
 */
public final class AtomicBigInteger {

    private final AtomicReference<BigInteger> bigInteger;

    public AtomicBigInteger(final BigInteger bigInteger) {
        this.bigInteger = new AtomicReference<>(Objects.requireNonNull(bigInteger));
    }
    public BigInteger incrementAndGet() {
        return bigInteger.accumulateAndGet(BigInteger.ONE, (previous, x) -> previous.add(x));
    }

    public BigInteger getAndIncrement() {
        return bigInteger.getAndAccumulate(BigInteger.ONE, (previous, x) -> previous.add(x));
    }

    public BigInteger get() {
        return bigInteger.get();
    }
}