package main.java.beans.data;


import java.io.Serializable;

/**
 * Created by Olesya on 07.05.2017.
 */
public final class Answer implements Serializable {

    private String message;
    private final Status status;
    private Object data;

    public Answer(Status status) {
        this.status = status;
    }

    public Answer(Status status, Object data) {
        this.status = status;
        this.data = data;
    }

    public Answer(Status status, String message) {
        this.message = message;
        this.status = status;
    }

    public Answer(Status status, String message, Object data) {
        this.message = message;
        this.status = status;
        this.data = data;
    }

    public String getMessage() {
        return this.message;
    }

    public Status getStatus() {
        return this.status;
    }

    public Object getData() {
        return data;
    }

}

