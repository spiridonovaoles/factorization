package main.java.service;

import main.java.beans.AtomicBigInteger;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Olesya on 07.05.2017.
 */
@Service
public class FactorizeService {

    /**
     * Метод для разложения чисел на простые множители
     *
     * @param number
     * @return
     */
    public static CopyOnWriteArrayList<AtomicBigInteger> doFactorizate(BigInteger number) {
        BigInteger two = BigInteger.valueOf(2);
        CopyOnWriteArrayList<AtomicBigInteger> primefactors = new CopyOnWriteArrayList<AtomicBigInteger>();

        if (number.compareTo(two) < 0) {
            primefactors.add(new AtomicBigInteger(BigInteger.ONE));
        }

        while (number.mod(two).equals(BigInteger.ZERO)) {
            primefactors.add(new AtomicBigInteger(two));
            number = number.divide(two);
        }

        if (number.compareTo(BigInteger.ONE) > 0) {
            BigInteger f = BigInteger.valueOf(3);
            while (f.multiply(f).compareTo(number) <= 0) {
                if (number.mod(f).equals(BigInteger.ZERO)) {
                    primefactors.add(new AtomicBigInteger(f));
                    number = number.divide(f);
                } else {
                    f = f.add(two);
                }
            }
            primefactors.add(new AtomicBigInteger(number));
        }
        return primefactors;
    }
}
