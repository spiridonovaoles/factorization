<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Factorization</title>
    <meta name="description" content="Factorization">
    <meta name="keywords" content="factorization">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script src="<c:url value="/resources/js/jquery-1.10.2.min.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/factorize.js"/>" type="text/javascript"></script>
</head>
<body>
<div class="container-fluid">
    <h4>Разложение на простые множители</h4>
    Введите числа:
    <div class="dev_input_block">
        <div class="dev_input_block_item">
            <div class="form-group">
                <input type="number" class="dev_input_number form-control" style="width: auto; display: inline;" value="">
                <button type="button" class="dev_delete_button btn btn-info" style="display: inline-block;">удалить</button>
            </div>
            <label class="dev_result_label" style="display: none;">Ответ:</label> <span class="dev_result"></span>
        </div>
    </div>
    <button type="button" class="dev_add_button btn btn-">Добавить</button>
    <button type="button" class="dev_factorization btn btn-success">Разложить</button>
</div>
<div class="dev_input_block_item dev_input_block_item_template" style="display:none;">
    <div class="form-group">
        <input type="number" class="dev_input_number form-control" style="width: auto; display: inline;" value="">
        <button type="button" class="dev_delete_button btn btn-info" style="display: inline-block;">Удалить</button>
    </div>
    <label class="dev_result_label" style="display: none;">Ответ:</label> <span class="dev_result"></span>
</div>

</body>